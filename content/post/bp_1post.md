---
author: "Slowpoke_Rodriguez"
date: 2016-04-11
linktitle: Первая-запись
menu:
  main:
    parent: Содержание
next: Тематика-блога
title: Первая запись
weight: 10
---

Изучаю генератор статического сайта на питоне [Pelican](http://blog.getpelican.com/).
Попробую разместить сгенерированный им контент через репозиторий 
[Bitbucket.org](https://bitbucket.org/Slowpoke_Rodriguez/oneccodeeditorblog) на сайт 
[aerobatic.io](https://onec-code-editor.aerobatic.io/).
